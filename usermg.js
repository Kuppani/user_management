const express = require('express');
const path =require('path');
const bodyParser= require('body-parser');
//const cors = require('cors');


//init app
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));



//Routes
require('./routes/routes')(app);


// load view Engin

app.set('views',__dirname+'/views');
app.set('view engine','ejs');
//app.engine('html',require('ejs').renderFile);

// app.configure(function(){
//     app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
// });



app.listen(7086,function(){
    console.log("server starts on port 7086");
});
