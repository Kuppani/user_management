I.PROJECT NAME

Cadenza_user_management

II.PREREQUISITES

Node.js
Node Package Manager(npm)

III.Running Locally

git clone https://gitlab.com/Kuppani/user_management.git
cd user_management
npm install
npm start or node app.js or nodemon start app.js

Your app should now be running on localhost:7086